package steps

const (
	EmptyStep      = "step is empty"
	StepFailed     = "step %s failed: %v"
	StepNotMatched = "one or more steps does not matched any known handler"
)
